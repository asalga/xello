'use strict';

module.exports = function(grunt) {

    const Livereload = 35729;
    const ServeStatic = require('serve-static');

    const src = 'src';
    const data = 'data';
    const tmp = '.tmp';
    const app = 'app';
    const sassCache = '.sass-cache';

    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({

        /**
         *
         */
        copy: {
            dev: {
                files: [
                    // HTML
                    {
                        expand: true,
                        cwd: `${src}/`,
                        src: `index.html`,
                        dest: `${app}/`,
                        filter: 'isFile'
                    },
                    // CSS
                    {
                        expand: true,
                        cwd: `${src}/css`,
                        src: `*.*`,
                        dest: `${app}/css`,
                        filter: 'isFile'
                    },
                    // JS
                    {
                        expand: true,
                        cwd: `${src}/js`,
                        src: `*.js`,
                        dest: `${app}/js`,
                        filter: 'isFile'
                    }
                ]
            }
        },


        /**
         * https://github.com/jsoverson/grunt-open
         * Opens the web server in the browser
         */
        open: {
            server: {
                path: `http://localhost:<%= connect.options.port %>/`
            }
        },


        /** 
         */
        clean: {
            build: {
                src: [
                    `${app}`,
                    `${tmp}`,
                    `${sassCache}`
                ]
            }
        },

        /**
         * 
         */
        sass: {
            dev: {
                options: {
                    style: 'nested',
                    sourcemap: 'auto',
                    update: true
                },
                files: [{
                    dest: `${app}/css/style.css`,
                    src: `${src}/scss/main.scss`
                }]
            }
        },

        /**
         * https://github.com/gruntjs/grunt-contrib-connect
         */
        connect: {
            options: {
                port: 9000,
                hostname: '*'
            },
            livereload: {
                options: {
                    middleware: function(connect, options) {
                        return [
                            ServeStatic(`${app}`),
                            connect().use(`${app}`, ServeStatic(`${app}`)),
                            ServeStatic(`${app}`)
                        ]
                    }
                }
            }
        },

        /**
         * https://github.com/gruntjs/grunt-contrib-watch
         */
        watch: {
            options: {
                spawn: true,
                livereload: true
            },
            all: {
                files: [
                    `${src}/**/*.*`
                ],
                tasks: [
                    'sass',
                    'copy'
                ],
                options: {
                    livereload: true
                }
            }
        }
    });

    grunt.registerTask('default', [
        'copy',
        'sass',
        'connect:livereload',
        'open',
        'watch'
    ]);
};