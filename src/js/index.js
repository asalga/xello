'use strict';

const SlotTemplate = `<div class='col-sm-12'>
                        <div class='empty-slot'>Available Slot</div>
                      </div>`;

$(document).ready(function() {

    let prereqContainer = $(`<div class='prereqContainer'>
                             <div class='prereq'>
                               <span class='course-name'>This course requires a Pre-Requisite.</span>
                               <a href='#'>Add one.</a>
                               <span class='notch'></span>
                             </div>
                           </div>`);

    let yearContainer = $('.sem-card-future .slots');
    for (let i = 0; i < 8; ++i) {
        let slot = $(SlotTemplate).clone();
        yearContainer.append(slot);
    }

    $('#target').hover(function() {
        if ($('.prereqContainer').length >= 1) {
            return;
        }

        prereqContainer.insertBefore($(this));

        // Make the width match so it looks nice and aligned
        $('.prereq').css({'width': $(this).css('width') })

    }, function() {
        $('.prereqContainer').remove();
    });
});